import kotlin.test.assertEquals
import org.junit.Test

class HomeControllerTest {
  @Test fun JSONPairs() {
    val JSON = getJSONPairs(listOf("a" to "b", "c" to "d"))
    assertEquals("""
    |{
    |  "a": "b",
    |  "c": "d"
    |}""".trimMargin(), JSON)
  }

  @Test fun JSONPairsEmptyParam() {
    val JSON = getJSONPairs(listOf())
    assertEquals("{}", JSON);
  }
}
