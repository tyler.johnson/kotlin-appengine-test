import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import com.beust.klaxon.json

@WebServlet(name = "echo-headers", value = "/")
class HomeController : HttpServlet() {
  override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    resp.setContentType("application/json")

    resp.writer.write(getJSONPairs(req.getHeaderNames().toList().map {
      it.toString() to req.getHeader(it)
    }))
  }
}

fun getJSONPairs(pairs: List<Pair<String, *>>) = json {
  obj(*pairs.toTypedArray())
}.toJsonString(true)
